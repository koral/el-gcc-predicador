;;; el-gcc-predicador.el --- Make use of rtl predicates -*- lexical-binding: t; -*-

;; Maintainer: andrea_corallo@yahoo.it
;; Package: el-gcc-predicador
;; Homepage: https://gitlab.com/koral/el-gcc-predicador
;; Version: 0.1
;; Package-Requires: ((emacs "24"))
;; Keywords:

;; This file is not part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;; Try to find missed usecases for gcc rtl predicates into gcc source code and
;; fix them.

;;; Code:

(require 'rx)

(defconst gccpr-regexp
  (rx (or (seq
	   "GET_CODE" (zero-or-more space) "("
	   (group-n 1 (one-or-more (any "A-Za-z_"))) ")"
	   (zero-or-more space)
	   (group-n 2 (or "==" "!="))
	   (zero-or-more space)
	   (group-n 3 (one-or-more (any "A-Za-z_"))))
	  (seq
	   (group-n 3 (one-or-more (any "A-Za-z_")))
	   (zero-or-more space)
	   (group-n 2 (or "==" "!="))
	   (zero-or-more space)
	   "GET_CODE" (zero-or-more space) "("
	   (group-n 1 (one-or-more (any "A-Za-z_"))) ")")))
  "Regexp matching predicate candidates.")

(defconst gccpr-preds
  '(("REG" . "REG_P")
    ("MEM" . "MEM_P")
    ("CONST_INT" . "CONST_INT_P")
    ("CONST_WIDE_INT" . "CONST_WIDE_INT_P")
    ("CONST_FIXED" . "CONST_FIXED_P")
    ("CONST_DOUBLE" . "CONST_DOUBLE_P")
    ("CODE_LABEL" . "LABEL_P")
    ("JUMP_INSN" . "JUMP_P")
    ("CALL_INSN" . "CALL_P")
    ("INSN" . "NONJUMP_INSN_P")
    ("DEBUG_INSN" . "DEBUG_INSN_P")
    ("NOTE" . "NOTE_P")
    ("BARRIER" . "BARRIER_P")
    ("JUMP_TABLE_DATA" . "JUMP_TABLE_DATA_P")
    ("SYMBOL_REF" . "SYMBOL_REF_P")
    ("SUBREG" . "SUBREG_P")
    ("SYMBOL_REF" . "SYMBOL_REF_P")
    ("LABEL_REF" . "LABEL_REF_P"))
  "Alist of simple CODE . PREDICATE to be transformed.")

(defun gccpr-run ()
  "Do the actual job of replacing rtl code checks with corresponding
 predicates."
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (while (re-search-forward gccpr-regexp nil t)
      (let ((var (match-string 1))
	    (op (match-string 2))
	    (code (match-string 3)))
	(catch 'next
	  (mapc (lambda (pred)
		  (when (string= code (car pred))
		    (kill-region (match-beginning 0) (match-end 0))
		    (when (string= op "!=")
		      (insert "!"))
		    (insert (cdr pred) " (" var ")")
		    (throw 'next t)))
		gccpr-preds))))))

(defun gccpr-run-on-folder (folder &optional recursive)
  "Run el predicador on a FOLDER possibly in a RECURSIVE fashion."
  (interactive "D")
  (let ((before-save-hook nil)
	(init-time (current-time)))
    (mapc (lambda (f)
	    (with-temp-file f
	      (insert-file-contents f)
	      (gccpr-run)
	      (message "Done with %s" f)))
	  (if recursive
	      (directory-files-recursively folder ".*\\.c$")
	    (directory-files folder t ".*\\.c$")))
    (message "Served you in %f sec" (float-time (time-since init-time)))))

;;; el-gcc-predicador.el ends here
